@extends("layouts.app")

@section("tagline")
    for smart homes of wise people
@endsection

@section("pagetitle")
    Welcome to Home Asset Management
@endsection

@section("content")
    <ul>
        @foreach ($tasks as $task)
            <li>{{ $task }}</li>
        @endforeach
    </ul>
@endsection