<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Asset extends Model
{
	use SoftDeletes;
    public function category(){
    	return $this->belongsTo("\App\Category");
    }
    public function type(){
    	return $this->belongsTo("\App\Type");
    }
}