<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('typeid');
            $table->unsignedBigInteger('categoryid');
            $table->unsignedBigInteger('locationid');
            $table->string('description');
            $table->string('img_path1');
            $table->string('img_path2');
            $table->string('img_path3');
            $table->string('img_path4');
            $table->string('img_path5');
            $table->string('img_path6');
            $table->timestamps();

            $table->foreign('type_id')
                ->references('id')->on('types')
                ->onDelete('restrict')
                ->onUpdate('cascade');
            $table->foreign('category_id')
                ->references('id')->on('categories')
                ->onDelete('restrict')
                ->onUpdate('cascade');
            $table->foreign('location_id')
                ->references('id')->on('locations')
                ->onDelete('restrict')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assets');
    }
}
